#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "maths.cpp"

TEST_CASE("test primes", "[is_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);
  REQUIRE(is_prime(6) == false);
  REQUIRE(is_prime(7) == true);
  REQUIRE(is_prime(8) == false);
  REQUIRE(is_prime(9) == false);
}

TEST_CASE("test absolute", "[absolute]")
{
	REQUIRE(absolute(10) == 10);
	REQUIRE(absolute(-10) == 10);
	REQUIRE(absolute(0) == 0);
}

TEST_CASE("test power", "[power]")
{
	REQUIRE(power(3, 3) == 27);
	REQUIRE(power(2, 4) == 16);
	REQUIRE(power(1, 1) == 1);
}
